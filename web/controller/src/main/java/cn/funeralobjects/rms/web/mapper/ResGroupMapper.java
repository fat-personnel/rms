package cn.funeralobjects.rms.web.mapper;

import cn.funeralobjects.common.web.controller.converter.ModelMapper;
import cn.funeralobjects.rms.entity.EResGroup;
import cn.funeralobjects.rms.model.ResGroup;
import cn.funeralobjects.rms.web.model.MResGroup;
import cn.funeralobjects.rms.web.model.VResGroup;
import org.mapstruct.Mapper;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 11:49 AM
 */
@Mapper(componentModel = "spring")
public interface ResGroupMapper extends ModelMapper<MResGroup, ResGroup, EResGroup, VResGroup> {
}
