package cn.funeralobjects.rms.web.mapper;

import cn.funeralobjects.rms.model.ResOssUploadInfo;
import cn.funeralobjects.rms.web.model.VResOssUploadInfo;
import org.mapstruct.Mapper;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 11:15 AM
 */
@Mapper(componentModel = "spring")
public interface ResOssUploadInfoMapper {

    VResOssUploadInfo toVo(ResOssUploadInfo dto);
}
