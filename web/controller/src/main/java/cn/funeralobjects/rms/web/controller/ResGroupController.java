package cn.funeralobjects.rms.web.controller;

import cn.funeralobjects.common.service.CommonCloudCrudService;
import cn.funeralobjects.common.web.controller.CommonCloudCrudController;
import cn.funeralobjects.common.web.controller.converter.ModelMapper;
import cn.funeralobjects.rms.entity.EResGroup;
import cn.funeralobjects.rms.model.ResGroup;
import cn.funeralobjects.rms.service.ResGroupService;
import cn.funeralobjects.rms.web.api.ResGroupApi;
import cn.funeralobjects.rms.web.mapper.ResGroupMapper;
import cn.funeralobjects.rms.web.model.MResGroup;
import cn.funeralobjects.rms.web.model.VResGroup;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 11:46 AM
 */
@RestController
@RequestMapping("/res-group")
public class ResGroupController implements ResGroupApi, CommonCloudCrudController<VResGroup, MResGroup, ResGroup, EResGroup, Integer> {

    @Resource
    private ResGroupService resGroupService;

    @Resource
    private ResGroupMapper resGroupMapper;

    @Override
    public CommonCloudCrudService<ResGroup, EResGroup, Integer> commonCloudCrudService() {
        return resGroupService;
    }

    @Override
    public ModelMapper<MResGroup, ResGroup, EResGroup, VResGroup> modelMapper() {
        return resGroupMapper;
    }
}
