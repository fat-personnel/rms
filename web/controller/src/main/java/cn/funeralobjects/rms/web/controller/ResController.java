package cn.funeralobjects.rms.web.controller;

import cn.funeralobjects.rms.service.ResOssService;
import cn.funeralobjects.rms.web.api.ResApi;
import cn.funeralobjects.rms.web.mapper.ResOssUploadInfoMapper;
import cn.funeralobjects.rms.web.model.VResOssUploadInfo;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 11:10 AM
 */
@RestController
@RequestMapping("/res")
public class ResController implements ResApi {

    @Resource
    private ResOssService resOssService;

    @Resource
    private ResOssUploadInfoMapper resOssUploadInfoMapper;

    @PostMapping("/{groupId}/apply/upload")
    @Override
    public Mono<VResOssUploadInfo> applyUpload(@PathVariable Integer groupId,
                                               @RequestParam("sts-prod-res-id") Integer stsProdResId,
                                               @RequestParam("bucket-code") String bucketCode,
                                               @RequestParam("name") String name,
                                               @RequestParam(value = "path", required = false) String path) {
        return Mono.defer(() -> path == null ? resOssService.applyUpload(groupId, stsProdResId, bucketCode, name)
                : resOssService.applyUpload(groupId, stsProdResId, bucketCode, name, path))
                .map(resOssUploadInfoMapper::toVo);
    }

    @PutMapping("/confirm-upload/{id}")
    @Override
    public Mono<Void> confirmUpload(@PathVariable Integer id) {
        return Mono.just(id)
                .flatMap(resOssService::confirmUpload);
    }
}
