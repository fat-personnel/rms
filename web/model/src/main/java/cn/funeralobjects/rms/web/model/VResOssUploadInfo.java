package cn.funeralobjects.rms.web.model;

import cn.funeralobjects.aoss.web.model.VBucketStsAccessKey;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 11:01 AM
 */
@Data
@Accessors(chain = true)
public class VResOssUploadInfo {
    private Integer uploadId;
    private String uploadCode;
    private String uploadPath;
    private VBucketStsAccessKey bucketStsAccessKey;
}
