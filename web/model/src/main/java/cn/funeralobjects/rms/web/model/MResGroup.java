package cn.funeralobjects.rms.web.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 11:47 AM
 */
@Data
@Accessors(chain = true)
public class MResGroup {
    @NotEmpty
    private String code;
    @NotEmpty
    private String name;
    private String remark;
}
