package cn.funeralobjects.rms.web.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 11:44 AM
 */
@Data
@Accessors(chain = true)
public class VResGroup {
    private Integer id;
    private String code;
    private String name;
    private String remark;
    private Date createTime;
}
