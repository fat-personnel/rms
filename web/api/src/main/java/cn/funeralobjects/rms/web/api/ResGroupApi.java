package cn.funeralobjects.rms.web.api;

import cn.funeralobjects.common.web.api.CommonCloudCrudRestApi;
import cn.funeralobjects.rms.web.model.MResGroup;
import cn.funeralobjects.rms.web.model.VResGroup;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 11:43 AM
 */
public interface ResGroupApi extends CommonCloudCrudRestApi<VResGroup, MResGroup, Integer> {
}
