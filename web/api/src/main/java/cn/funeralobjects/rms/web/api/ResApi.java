package cn.funeralobjects.rms.web.api;

import cn.funeralobjects.rms.web.model.VResOssUploadInfo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import reactor.core.publisher.Mono;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 11:04 AM
 */
@Validated
public interface ResApi {

    @PostMapping("/{groupId}/apply/upload")
    Mono<VResOssUploadInfo> applyUpload(@PathVariable Integer groupId,
                                        @RequestParam("sts-prod-res-id") Integer stsProdResId,
                                        @RequestParam("bucket-code") String bucketCode,
                                        @RequestParam("name") String name,
                                        @RequestParam(value = "path", required = false) String path);

    @PutMapping("/confirm-upload/{id}")
    Mono<Void> confirmUpload(@PathVariable Integer id);
}
