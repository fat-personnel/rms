CREATE SCHEMA IF NOT EXISTS funeralobjects_rms DEFAULT CHARSET UTF8MB4 COLLATE UTF8MB4_BIN;
USE funeralobjects_rms;

DROP TABLE IF EXISTS t_res_upload_record;
DROP TABLE IF EXISTS t_res_oss_info;
DROP TABLE IF EXISTS t_res_info;
DROP TABLE IF EXISTS t_res;
DROP TABLE IF EXISTS t_res_group;

CREATE TABLE IF NOT EXISTS t_res_group
(
    id          BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '主键id',
    name        VARCHAR(255) NOT NULL COMMENT '资源组名称',
    code        VARCHAR(50)  NOT NULL COMMENT '唯一编号',
    remark      VARCHAR(255) COMMENT '备注',
    enable      BIT(1)       NOT NULL DEFAULT 1 COMMENT '数据状态, 0:禁用，1:启用',
    commit      BIT(1)       NOT NULL DEFAULT 0 COMMENT '数据提交状态, 1:已确认/提交，0:未确认/确认中',
    del_salt    INT UNSIGNED          DEFAULT 0 COMMENT '删除标记/盐',
    create_time DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据创建时间',
    UNIQUE (name, del_salt),
    UNIQUE (code),
    INDEX (enable),
    INDEX (commit),
    INDEX (del_salt)
) ENGINE = InnoDB
  DEFAULT COLLATE = UTF8MB4_BIN COMMENT '资源组表';

CREATE TABLE IF NOT EXISTS t_res
(
    id           BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT COMMENT '主键id',
    code         VARCHAR(50)      NOT NULL COMMENT '唯一编号',
    bucket_code  VARCHAR(50)      NOT NULL COMMENT '所在资源包编码',
    res_group_id BIGINT UNSIGNED  NOT NULL COMMENT '所属资源组',
    enable       BIT(1)           NOT NULL DEFAULT 1 COMMENT '数据状态, 0:禁用，1:启用',
    commit       BIT(1)           NOT NULL DEFAULT 0 COMMENT '数据提交状态, 1:已确认/提交，0:未确认/确认中',
    name         VARCHAR(255)     NOT NULL COMMENT '资源名称',
    status       TINYINT UNSIGNED NOT NULL COMMENT '资源状态',
    del_salt     INT UNSIGNED              DEFAULT 0 COMMENT '删除标记/盐',
    create_time  DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '数据创建时间',
    update_time  TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据更新时间',
    FOREIGN KEY (res_group_id) REFERENCES t_res_group (id),
    UNIQUE (code),
    INDEX (enable),
    INDEX (commit),
    INDEX (bucket_code),
    INDEX (del_salt)
) ENGINE = InnoDB
  DEFAULT COLLATE = UTF8MB4_BIN COMMENT '资源表';

CREATE TABLE IF NOT EXISTS t_res_info
(
    res_id       BIGINT UNSIGNED PRIMARY KEY COMMENT '资源id',
    content_md5  VARCHAR(255) NOT NULL COMMENT 'md5',
    content_type VARCHAR(30)  NOT NULL COMMENT '资源类型',
    vol_level    TINYINT(3)   NOT NULL COMMENT '容量等级',
    last_active  DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次访问日期',
    active_count BIGINT UNSIGNED       DEFAULT 0 NOT NULL COMMENT '资源累计访问次数',
    FOREIGN KEY (res_id) REFERENCES t_res (id),
    INDEX (vol_level),
    INDEX (content_type)
) ENGINE = InnoDB
  DEFAULT COLLATE = UTF8MB4_BIN COMMENT '资源表信息表';


CREATE TABLE IF NOT EXISTS t_res_oss_info
(
    res_id      BIGINT UNSIGNED PRIMARY KEY COMMENT '资源id',
    url         VARCHAR(255)    NOT NULL COMMENT '资源所在路径',
    bucket      VARCHAR(255)    NOT NULL COMMENT '资源所在bucket',
    size        BIGINT UNSIGNED NOT NULL COMMENT '文件大小',
    e_tag       VARCHAR(32)     NOT NULL COMMENT 'oss-ETag',
    last_modify DATETIME        NOT NULL COMMENT '最后修改日期',
    FOREIGN KEY (res_id) REFERENCES t_res (id),
    INDEX (bucket)
) ENGINE = InnoDB
  DEFAULT COLLATE = UTF8MB4_BIN COMMENT '阿里OSS资源信息表';

CREATE TABLE IF NOT EXISTS t_res_upload_record
(
    res_id INT UNSIGNED PRIMARY KEY COMMENT '主键id',
    path   VARCHAR(255) NOT NULL COMMENT '上传文件所在路径'
) ENGINE = InnoDB
  DEFAULT COLLATE = UTF8MB4_BIN COMMENT '上传记录表';