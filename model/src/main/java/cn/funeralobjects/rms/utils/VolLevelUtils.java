package cn.funeralobjects.rms.utils;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 12:47 AM
 */
public class VolLevelUtils {

    private static final short MIN_SIZE = 4;
    private static final short VOL_LEVEL_MULTIPLE = 2;

    public static short getVolLevel(long size) {
        size = size / 1024;
        short i = 1;
        while (size > MIN_SIZE) {
            size = size / VOL_LEVEL_MULTIPLE;
            i++;
        }
        return i;
    }
}
