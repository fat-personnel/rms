package cn.funeralobjects.rms.enums;

import lombok.Getter;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 7:53 AM
 */
public enum UploadType {
    /**
     * 平台资源
     */
    PLATFORM_RESOURCE((short) 1),
    /**
     * 公开资源
     */
    PUBLIC_RESOURCE((short) 2),
    /**
     * 个人资源
     */
    PERSONNEL_RESOURCE((short) 3);

    @Getter
    private final Short num;

    UploadType(Short num) {
        this.num = num;
    }

    public static UploadType of(Number num) {
        if (num == null) {
            return null;
        }
        for (UploadType value : UploadType.values()) {
            if (value.num.equals(num.shortValue())) {
                return value;
            }
        }
        return null;
    }
}
