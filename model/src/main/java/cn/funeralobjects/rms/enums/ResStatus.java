package cn.funeralobjects.rms.enums;

import cn.funeralobjects.model.EnumNumberPair;
import lombok.Getter;

/**
 * 资源状态
 *
 * @author FuneralObjects
 * Create date: 2020/6/5 4:26 PM
 */
public enum ResStatus implements EnumNumberPair<ResStatus, Short> {
    /**
     * 等待上传
     */
    WAITING_UPLOAD((short) 1),
    /**
     * 正常
     */
    NORMAL((short) 2),
    ;

    /**
     * 编号
     */
    @Getter
    private final Short num;

    ResStatus(Short num) {
        this.num = num;
    }

    @Override
    public ResStatus getContent() {
        return this;
    }

    @Override
    public Short getNumber() {
        return this.num;
    }
}
