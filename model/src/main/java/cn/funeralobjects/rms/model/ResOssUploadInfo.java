package cn.funeralobjects.rms.model;

import cn.funeralobjects.aoss.web.model.VBucketStsAccessKey;
import cn.funeralobjects.util.annotation.ArgHasLength;
import cn.funeralobjects.util.annotation.ArgNotZero;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 7:51 AM
 */
@Data
@Accessors(chain = true)
public class ResOssUploadInfo {
    @ArgNotZero
    private Integer uploadId;
    private String uploadCode;
    @ArgHasLength
    private String uploadPath;
    private VBucketStsAccessKey bucketStsAccessKey;
}
