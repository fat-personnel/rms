package cn.funeralobjects.rms.model;

import cn.funeralobjects.rms.enums.ResStatus;
import cn.funeralobjects.util.annotation.ArgHasLength;
import cn.funeralobjects.util.annotation.ArgNonNull;
import cn.funeralobjects.util.annotation.ArgNotZero;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author FuneralObjects
 * Create date: 2020/6/1 8:09 AM
 */
@Data
@Accessors(chain = true)
public class Res {

    /**
     * 资源编号
     */
    @ArgHasLength
    private String code;

    /**
     * 资源上传时的名称
     */
    @ArgHasLength
    private String name;
    /**
     * 所在资源包编码
     */
    @ArgHasLength
    private String bucketCode;

    /**
     * 资源状态
     */
    @ArgNonNull
    private ResStatus status;

    /**
     * 资源组id
     */
    @ArgNotZero
    private Integer resGroupId;

}
