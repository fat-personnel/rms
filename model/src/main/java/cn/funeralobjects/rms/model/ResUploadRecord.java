package cn.funeralobjects.rms.model;

import cn.funeralobjects.rms.enums.ResStatus;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 5:53 PM
 */
@Data
@Accessors(chain = true)
public class ResUploadRecord {
    private Integer id;
    private String code;
    private String name;
    private String bucketCode;
    private ResStatus status;
    private String path;
}
