package cn.funeralobjects.rms.model;

import cn.funeralobjects.util.annotation.ArgHasLength;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 9:15 AM
 */
@Data
@Accessors(chain = true)
public class ResGroup {
    @ArgHasLength
    private String code;
    @ArgHasLength
    private String name;
    private String remark;
}
