package cn.funeralobjects.rms.model;

import cn.funeralobjects.util.annotation.ArgHasLength;
import cn.funeralobjects.util.annotation.ArgNonNull;
import cn.funeralobjects.util.annotation.ArgNotZero;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author FuneralObjects
 * Create date: 2020/6/5 4:46 PM
 */
@Data
@Accessors(chain = true)
public class ResOssInfo {

    @ArgNotZero
    private Integer resId;
    @ArgHasLength
    private String url;
    @ArgHasLength
    private String bucket;
    @ArgNonNull
    private Long size;
    @ArgHasLength
    private String eTag;
    private String lastModify;
}
