package cn.funeralobjects.rms.model;

import cn.funeralobjects.util.annotation.ArgHasLength;
import cn.funeralobjects.util.annotation.ArgNonNull;
import cn.funeralobjects.util.annotation.ArgNotZero;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author FuneralObjects
 * Create date: 2020/6/1 8:12 AM
 */
@Data
@Accessors(chain = true)
public class ResInfo {
    /**
     * 资源id
     */
    @ArgNotZero
    private Integer resId;

    /**
     * 后缀名
     */
    @ArgHasLength
    private String suffix;
    /**
     * md5
     */
    @ArgHasLength
    private String md5;
    /**
     * 长度
     */
    @ArgNonNull
    private Long length;
    /**
     * 容量等级
     */
    @ArgNonNull
    private Short volLevel;
    /**
     * 最后检查时间
     */
    private Date lastActive;
}
