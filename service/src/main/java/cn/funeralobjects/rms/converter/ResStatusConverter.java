package cn.funeralobjects.rms.converter;

import cn.funeralobjects.rms.enums.ResStatus;
import cn.funeralobjects.util.converter.BaseEnumNumberConverter;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 12:42 AM
 */
public class ResStatusConverter extends BaseEnumNumberConverter<ResStatus, Short> {
}
