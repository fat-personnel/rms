package cn.funeralobjects.rms.service.impl;

import cn.funeralobjects.common.jpa.service.CommonCloudCrudJpaService;
import cn.funeralobjects.common.repository.CommonCloudRepository;
import cn.funeralobjects.common.service.annotataion.CommonService;
import cn.funeralobjects.common.service.exception.DataConflictException;
import cn.funeralobjects.rms.entity.EResGroup;
import cn.funeralobjects.rms.model.ResGroup;
import cn.funeralobjects.rms.repository.EResGroupRepository;
import cn.funeralobjects.rms.service.ResGroupService;
import cn.funeralobjects.util.Assert;
import cn.funeralobjects.util.NumberUtils;
import cn.funeralobjects.util.annotation.ArgNotZero;
import cn.funeralobjects.util.annotation.AssertArg;

import javax.annotation.Resource;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 10:46 AM
 */
@CommonService
public class ResGroupServiceImpl implements ResGroupService, CommonCloudCrudJpaService<ResGroup, EResGroup, Integer> {
    @Resource
    private EResGroupRepository eResGroupRepository;

    @Override
    public CommonCloudRepository<EResGroup, Integer> commonCloudRepository() {
        return eResGroupRepository;
    }

    @AssertArg
    @Override
    public void checkConflict(@AssertArg ResGroup resGroup) throws DataConflictException {
        this.mustNotConflictByCode(resGroup.getCode());
        this.mustNotConflictByName(resGroup.getName());
    }

    @Override
    public EResGroup toEntity(ResGroup resGroup) {
        EResGroup group = new EResGroup();
        group
                .setRemark(resGroup.getRemark())
                .setCode(resGroup.getCode())
                .setName(resGroup.getName());
        return group;
    }

    @AssertArg
    @Override
    public void checkConflict(@AssertArg ResGroup resGroup, @ArgNotZero Integer withoutId) throws DataConflictException {
        this.mustNotConflictByCode(resGroup.getCode(), withoutId);
        this.mustNotConflictByName(resGroup.getName(), withoutId);
    }

    @Override
    public void copyToEntity(ResGroup sourceModify, EResGroup targetEntity) {
        targetEntity.setCode(sourceModify.getCode())
                .setName(targetEntity.getName());
    }

    @Override
    public boolean isValid(Integer id) {
        return NumberUtils.notZero(id);
    }

    @Override
    public void validateModify(ResGroup resGroup) {
        Assert.argAssert(resGroup, "resGroup");
    }
}
