package cn.funeralobjects.rms.service;

import cn.funeralobjects.rms.model.ResOssUploadInfo;
import reactor.core.publisher.Mono;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 10:09 AM
 */
public interface ResOssService {
    Mono<ResOssUploadInfo> applyUpload(Integer groupId, Integer stsProdResId, String bucketCode, String name, String path);

    Mono<ResOssUploadInfo> applyUpload(Integer groupId, Integer stsProdResId, String bucketCode, String name);

    Mono<Void> confirmUpload(Integer resId);

}
