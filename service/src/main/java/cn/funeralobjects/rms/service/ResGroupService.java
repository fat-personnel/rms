package cn.funeralobjects.rms.service;

import cn.funeralobjects.common.service.CommonCloudCrudService;
import cn.funeralobjects.rms.BasicInfo;
import cn.funeralobjects.rms.entity.EResGroup;
import cn.funeralobjects.rms.model.ResGroup;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 10:45 AM
 */
public interface ResGroupService extends CommonCloudCrudService<ResGroup, EResGroup, Integer> {
    String ENTITY_NAME = "ResGroup";

    @Override
    default String getName() {
        return ENTITY_NAME;
    }

    @Override
    default String getModuleName() {
        return BasicInfo.MODULE;
    }
}
