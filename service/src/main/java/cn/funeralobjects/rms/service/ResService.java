package cn.funeralobjects.rms.service;

import cn.funeralobjects.common.service.CommonCloudCrudService;
import cn.funeralobjects.rms.entity.ERes;
import cn.funeralobjects.rms.enums.ResStatus;
import cn.funeralobjects.rms.model.Res;
import cn.funeralobjects.rms.model.ResUploadRecord;

import java.util.Optional;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 1:40 AM
 */
public interface ResService extends CommonCloudCrudService<Res, ERes, Integer> {
    String ENTITY_NAME = "res";


    ERes add(Res res, String path);

    Optional<ResUploadRecord> getUploadRecord(Integer id);

    void updateStatus(Integer id, ResStatus status);
}
