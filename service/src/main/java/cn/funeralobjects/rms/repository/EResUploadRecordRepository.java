package cn.funeralobjects.rms.repository;

import cn.funeralobjects.rms.entity.EResUploadRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 2:19 PM
 */
public interface EResUploadRecordRepository extends JpaRepository<EResUploadRecord, Integer> {

}
