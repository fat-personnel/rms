package cn.funeralobjects.rms.repository;

import cn.funeralobjects.common.repository.CommonCloudRepository;
import cn.funeralobjects.rms.entity.EResGroup;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 10:44 AM
 */
public interface EResGroupRepository extends CommonCloudRepository<EResGroup, Integer> {
}
