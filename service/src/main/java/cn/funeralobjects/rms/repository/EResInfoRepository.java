package cn.funeralobjects.rms.repository;

import cn.funeralobjects.rms.entity.EResInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 9:43 AM
 */
public interface EResInfoRepository extends JpaRepository<EResInfo, Integer> {
}
