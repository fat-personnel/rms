package cn.funeralobjects.rms.repository;

import cn.funeralobjects.rms.entity.EResOssInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 10:08 AM
 */
public interface EResOssInfoRepository extends JpaRepository<EResOssInfo, Integer> {
}
