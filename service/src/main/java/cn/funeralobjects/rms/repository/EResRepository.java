package cn.funeralobjects.rms.repository;

import cn.funeralobjects.common.repository.CommonCloudRepository;
import cn.funeralobjects.rms.entity.ERes;
import cn.funeralobjects.rms.enums.ResStatus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 1:42 AM
 */
public interface EResRepository extends CommonCloudRepository<ERes, Integer> {

    @Modifying
    @Query("UPDATE ERes SET status = :status WHERE id =:id AND delSalt = :delSalt ")
    void updateStatusByIdAndDelSalt(ResStatus status, Integer id, Integer delSalt);
}
