package cn.funeralobjects.rms.entity;

import cn.funeralobjects.common.annotations.UpdateTime;
import cn.funeralobjects.common.jpa.entity.BaseCloudJpaEntity;
import cn.funeralobjects.rms.converter.ResStatusConverter;
import cn.funeralobjects.rms.enums.ResStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 12:23 AM
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_res")
@Data
@DynamicUpdate
@DynamicInsert
@Accessors(chain = true)
public class ERes extends BaseCloudJpaEntity<Integer> {
    /**
     * 资源状态
     */
    @Convert(converter = ResStatusConverter.class)
    private ResStatus status;
    /**
     * 资源包编码
     */
    private String bucketCode;
    /**
     * 更新时间
     */
    @UpdateTime
    private Date updateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "res_group_id", updatable = false)
    private EResGroup resGroup;

}
