package cn.funeralobjects.rms.entity;

import cn.funeralobjects.common.jpa.entity.BaseCloudJpaEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author FuneralObjects
 * Create date: 2020/6/10 10:40 AM
 */
@EqualsAndHashCode(callSuper = true)
@Table(name = "t_res_group")
@Entity
@DynamicUpdate
@DynamicInsert
@Data
@Accessors(chain = true)
public class EResGroup extends BaseCloudJpaEntity<Integer> {
    private String remark;
}
