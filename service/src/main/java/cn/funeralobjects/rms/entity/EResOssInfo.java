package cn.funeralobjects.rms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 1:36 AM
 */
@Data
@Accessors(chain = true)
@Table(name = "t_res_oss_info")
@Entity
public class EResOssInfo {
    @Id
    private Integer resId;
    /**
     * 地址
     */
    private String url;
    /**
     * 资源包
     */
    private String bucket;
    /**
     * 尺寸
     */
    private Long size;
    /**
     * etag标签
     */
    private String eTag;
    /**
     * 最终更新时间
     */
    private Date lastModify;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "res_id")
    private ERes res;
}
