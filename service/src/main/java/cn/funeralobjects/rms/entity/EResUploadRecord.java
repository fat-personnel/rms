package cn.funeralobjects.rms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author FuneralObjects
 * Create date: 2020/6/8 2:15 PM
 */
@Data
@Accessors(chain = true)
@Entity
@Table(name = "t_res_upload_record")
public class EResUploadRecord {
    @Id
    private Integer resId;
    private String path;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "res_id")
    private ERes res;
}
