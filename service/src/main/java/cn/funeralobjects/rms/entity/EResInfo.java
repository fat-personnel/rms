package cn.funeralobjects.rms.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author FuneralObjects
 * Create date: 2020/6/7 12:46 AM
 */
@Data
@Accessors(chain = true)
@Entity
@Table(name = "t_res_info")
@DynamicInsert
@DynamicUpdate
public class EResInfo {
    @Id
    private Integer resId;
    /**
     * md5值
     */
    private String contentMd5;
    /**
     * 后缀名
     */
    private String contentType;
    /**
     * 文件尺寸等级
     */
    private Short volLevel;
    /**
     * 该资源最后一次访问日期
     */
    private Date lastActive;

    /**
     * 资源累计访问次数
     */
    private Long activeCount;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "res_id")
    private ERes res;
}
